+++
title = "KiCad Version 7 Release Candidate 1"
date = "2023-01-08"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is excited to announce the first release candidate
for the upcoming version 7 stable release.  Version 7 will contain
many new features compared to the current version 6 release.  Please
consider giving 7.0.0-rc1 a try to discover any additional issues so
they can be fixed before release at the end of January.  Thank you to
everyone who contributed to version 7.

## How to Download Release Candidate Builds

The release candidate packages can be found in the "Nightly Development
Builds" folder on the https://www.kicad.org/download/[KiCad Download Page].

## Changes in Version 7

If you haven't been following the development of KiCad, check out the
developer highlights below for some of the new features included in
version 7.

* https://www.kicad.org/blog/2022/02/Development-Highlight-February-Update/[Development Highlight: February Update]
* https://www.kicad.org/blog/2022/04/Development-Highlight-April-Update/[Development Highlight: April Update]
* https://www.kicad.org/blog/2022/12/Development-Highlight-December-Edition-features-coming-to-KiCad-7/[Development Highlight: December Edition]

