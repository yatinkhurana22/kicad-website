+++
title = "TFGPS01: Drone multi-constellation GNSS receiver"
projectdeveloper = "ThunderFly s.r.o."
projecturl = "https://github.com/ThunderFly-aerospace/TFGPS01"
"made-with-kicad/categories" = [
    "Drones"
]
+++

TFGPS01 is a GNSS receiver designed for UAVs. The module has improved RF noise immunity and supports multiple navigation systems. Therefore the module is capable of receiving GPS, GLONASS, Galileo, and BeiDou navigation signals. It is designed primarily for use on small drones. The TFGPS can also function as a standalone UART GPS or as a USB GPS receiver.

The module has several features for UAVs. For example, it has a pass-through I2C port, payload interface, integrated beeper and safety LED or integrated connector for an external safety switch, and safety LED.
